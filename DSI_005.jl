println("Create a 2x4 two dimensional matrix with random floats in it and in the next step determine the biggest element.")
matrix = rand(2, 4)
show(STDOUT, "text/plain", matrix)
println()
println(findmax(matrix))


println("1. Create two matrices of the same layout and test if addition and subtraction of the matrix works as expected: C = A + B")
println("--------- A:")
A = rand(4,4)
show(STDOUT, "text/plain", A)
println()

println("--------- B:")
B = rand(4,4)
show(STDOUT, "text/plain", B)
println()

println("--------- A+B:")
C = A + B
show(STDOUT, "text/plain", C)
println()

println("2. Now compare matrix multiplication either this way A * B and this way A .* B. Whats the difference?!")
println("--------- A*B:")
MUL1 = A * B
show(STDOUT, "text/plain", MUL1)
println()

println("--------- B*A:")
MUL2 = B * A
show(STDOUT, "text/plain", MUL2)
println()

println("3. What about matrix division with / or \\ ?!")

println("--------- A/B:")
INV1 = A / B
show(STDOUT, "text/plain", INV1)
println()

println("--------- A\\B:")
INV2 = A \ B
show(STDOUT, "text/plain", INV2)
println()

println("--------- B/A:")
INV3 = B / A
show(STDOUT, "text/plain", INV3)
println()

println("--------- B\\A:")
INV4 = B \ A
show(STDOUT, "text/plain", INV4)
println()

println("4. Create a 3x3 integer matrix A with useful numbers. Now try A+1, A-1, A*2, A/2.")
intMat = [1 2 3; 4 5 6; 7 8 9]
show(STDOUT, "text/plain", intMat)
println()

println("--------- +1")
mat1 = intMat + 1
show(STDOUT, "text/plain", mat1)
println()

println("--------- -1")
mat2 = intMat - 1
show(STDOUT, "text/plain", mat2)
println()

println("--------- *2")
mat3 = intMat * 2
show(STDOUT, "text/plain", mat3)
println()

println("--------- /2")
mat4 = intMat / 2
show(STDOUT, "text/plain", mat4)
println()

println("5. Now multiply a 3x4 matrix with a suitable (4)vector.")
threeFour = rand(3,4)
show(STDOUT, "text/plain", threeFour)
println()

vec = threeFour * [1.0, 2.0, 3.0, 4.0]
show(STDOUT, "text/plain", vec)
println()
